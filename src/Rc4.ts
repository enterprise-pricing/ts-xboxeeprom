export interface StateHolder {
  state: Uint8Array;
  x: number;
  y: number;
}

export const swapBytes = (array: Uint8Array, index1: number, index2: number) => {
  const temp = array[index1];
  array[index1] = array[index2];
  array[index2] = temp;
};

export const resetState = (holder: StateHolder) => {
  holder.x = 0;
  holder.y = 0;

  for (let i = 0; i < 256; i++) {
    holder.state[i] = i;
  }
};

export class Rc4 {
  private readonly holder: StateHolder;

  constructor(private readonly key: Uint8Array) {
    this.holder = {
      state: new Uint8Array(256),
      x: 0,
      y: 0,
    };

    this.prepareKey();
  }

  public crypt(buffer: Uint8Array): Uint8Array {
    const temp = buffer.slice();
    this.cryptInner(temp);
    return temp;
  }

  private prepareKey() {
    let index1 = 0;
    let index2 = 0;
    resetState(this.holder);

    for (let i = 0; i < 256; i++) {
      index2 = (this.key[index1] + (this.holder.state[i] + index2)) % 256;
      swapBytes(this.holder.state, i, index2);

      index1 = (index1 + 1) % this.key.length;
    }
  }

  private cryptInner(buffer: Uint8Array) {
    const length = buffer.length;
    let x = this.holder.x;
    let y = this.holder.y;

    let xorIndex: number;

    for (let i = 0; i < length; i++) {
      x = (x + 1) % 256;
      y = (this.holder.state[x] + y) % 256;

      swapBytes(this.holder.state, x, y);
      xorIndex = (this.holder.state[x] + this.holder.state[y]) % 256;
      buffer[i] = buffer[i] ^ this.holder.state[xorIndex];
    }

    this.holder.x = x;
    this.holder.y = y;
  }
}
