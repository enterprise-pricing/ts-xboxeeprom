export type ByteSlice = Readonly<[number, number]>;

export interface EepromStructure {
  HmacSha1Hash: ByteSlice;
  Confounder: ByteSlice;
  HDDKkey: ByteSlice;
  HDDKkeyDecrypt: ByteSlice;
  XBERegion: ByteSlice;
  Checksum2: ByteSlice;
  SerialNumber: ByteSlice;
  MACAddress: ByteSlice;
  UNKNOWN2: ByteSlice;
  OnlineKey: ByteSlice;
  VideoStandard: ByteSlice;
  UNKNOWN3: ByteSlice;
  Checksum3: ByteSlice;
  TimeZoneBias: ByteSlice;
  TimeZoneStdName: ByteSlice;
  TimeZoneDltName: ByteSlice;
  UNKNOWN4: ByteSlice;
  TimeZoneStdDate: ByteSlice;
  TimeZoneDltDate: ByteSlice;
  UNKNOWN5: ByteSlice;
  TimeZoneStdBias: ByteSlice;
  TimeZoneDltBias: ByteSlice;
  LanguageID: ByteSlice;
  VideoFlags: ByteSlice;
  AudioFlags: ByteSlice;
  ParentalControlGames: ByteSlice;
  ParentalControlPwd: ByteSlice;
  ParentalControlMovies: ByteSlice;
  XBOXLiveIPAddress: ByteSlice;
  XBOXLiveDNS: ByteSlice;
  XBOXLiveGateWay: ByteSlice;
  XBOXLiveSubNetMask: ByteSlice;
  OtherSettings: ByteSlice;
  DVDPlaybackKitZone: ByteSlice;
  UNKNOWN6: ByteSlice;
}
/**
 * This is the structure of the EEPROM.
 */
export const Structure: EepromStructure = {
  HmacSha1Hash: [0x0, 20],
  Confounder: [0x14, 8],
  HDDKkey: [0x1c, 16],
  HDDKkeyDecrypt: [0x1c, 20],
  XBERegion: [0x2c, 4],
  Checksum2: [0x30, 4],
  SerialNumber: [0x34, 12],
  MACAddress: [0x40, 6],
  UNKNOWN2: [0x46, 2],
  OnlineKey: [0x48, 16],
  VideoStandard: [0x58, 4],
  UNKNOWN3: [0x5c, 4],
  Checksum3: [0x60, 4],
  TimeZoneBias: [0x64, 4],
  TimeZoneStdName: [0x68, 4],
  TimeZoneDltName: [0x5c, 4],
  UNKNOWN4: [0x70, 8],
  TimeZoneStdDate: [0x78, 4],
  TimeZoneDltDate: [0x7c, 4],
  UNKNOWN5: [0x80, 8],
  TimeZoneStdBias: [0x88, 4],
  TimeZoneDltBias: [0x8c, 4],
  LanguageID: [0x90, 4],
  VideoFlags: [0x94, 4],
  AudioFlags: [0x98, 4],
  ParentalControlGames: [0x9c, 4],
  ParentalControlPwd: [0xa0, 4],
  ParentalControlMovies: [0xa4, 4],
  XBOXLiveIPAddress: [0xa8, 4],
  XBOXLiveDNS: [0xac, 4],
  XBOXLiveGateWay: [0xb0, 4],
  XBOXLiveSubNetMask: [0xb4, 4],
  OtherSettings: [0xa8, 4],
  DVDPlaybackKitZone: [0xbc, 4],
  UNKNOWN6: [0xc0, 6],
};

export enum DvdZone {
  NONE = 0,
  ZONE1,
  ZONE2,
  ZONE3,
  ZONE4,
  ZONE5,
  ZONE6,
}

export enum XbeRegion {
  INVALID = 0,
  NORTH_AMERICA = 1,
  JAPAN = 2,
  EURO_AUSTRALIA = 4,
}

export type VideoStandard = Readonly<[number, number, number, number]>;

interface PossibleVideoStandards {
  INVALID: VideoStandard;
  NTSC_M: VideoStandard;
  PAL_I: VideoStandard;
}

export const VideoStandards: PossibleVideoStandards = {
  INVALID: [0, 0, 0, 0],
  NTSC_M: [0, 0x01, 0x40, 0],
  PAL_I: [0, 0x03, 0x80, 0],
};

export enum XboxVersion {
  NONE = 9,
  V1_0 = 10,
  V1_1 = 11,
  V1_2 = 12,
}

export const XBOX_VERSIONS = [XboxVersion.NONE, XboxVersion.V1_0, XboxVersion.V1_1, XboxVersion.V1_2];
