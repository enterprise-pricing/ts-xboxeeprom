import { calculateHddPassword, xboxHmacSha1 } from './CryptoUtils';
import { Rc4 } from './Rc4';
import { ByteSlice, Structure, VideoStandard, VideoStandards, XbeRegion, XBOX_VERSIONS, XboxVersion } from './Types';

const uint8ToArray = (input: Uint8Array): number[] => {
  const result: number[] = [];
  input.forEach(v => result.push(v));
  return result;
};

const arrayToHexString = (data: Uint8Array): string => {
  return uint8ToArray(data)
    .map(byte => byte.toString(16))
    .map(hex => (hex.length === 1 ? '0' + String(hex) : hex))
    .join('');
};

const stringToBytes = (model: string): Uint8Array => {
  const numbers = model.split('').map(str => str.charCodeAt(0));
  return new Uint8Array(numbers);
};

export const arrayEquals = (left: Uint8Array, right: Uint8Array): boolean => {
  for (let i = 0; i < left.length; i++) {
    if (left[i] !== right[i]) {
      return false;
    }
  }
  return true;
};

export const decryptEeprom = (raw: Uint8Array): Eeprom => {
  for (const version of XBOX_VERSIONS) {
    const eeprom = new Eeprom(raw.slice());

    const sha1part = eeprom.get(Structure.HmacSha1Hash);
    const baKeyHash = xboxHmacSha1(version, [new Uint8Array(sha1part)]);

    const rc4 = new Rc4(baKeyHash);
    const confounder = rc4.crypt(eeprom.get(Structure.Confounder));
    const hddKey = rc4.crypt(eeprom.get(Structure.HDDKkeyDecrypt));

    const confirmHash = xboxHmacSha1(version, [confounder, hddKey]);
    const expected = eeprom.get(Structure.HmacSha1Hash);
    if (arrayEquals(confirmHash, expected)) {
      // This is the correct version, set the decrypted values on the eeprom and return.
      eeprom.set(Structure.Confounder, confounder);
      eeprom.set(Structure.HDDKkeyDecrypt, hddKey);
      eeprom.version = version;
      return eeprom;
    }
  }

  throw new Error('Could not decrypt the EEPROM.');
};

/**
 * A class that interprets a byte array as an Xbox eeprom.
 * This assumes the bytes have been decrypted.
 */
export class Eeprom {
  public version?: XboxVersion;
  private readonly raw: Uint8Array;

  constructor(raw: Uint8Array) {
    if (raw.length !== 256) {
      throw Error('The EEPROM must be 256 bytes.');
    }
    this.raw = raw;
  }

  public getHddKey(): string {
    const data = this.get(Structure.HDDKkey);
    return arrayToHexString(data);
  }

  public getVideoStandard(): VideoStandard {
    const raw = this.get(Structure.VideoStandard);
    if (raw[0] !== 0 || raw[3] !== 0) {
      throw Error('Invalid video standard in EEPROM');
    }

    if (raw[1] === VideoStandards.NTSC_M[1] && raw[2] === VideoStandards.NTSC_M[2]) {
      return VideoStandards.NTSC_M;
    }

    if (raw[1] === VideoStandards.PAL_I[1] && raw[2] === VideoStandards.PAL_I[2]) {
      return VideoStandards.PAL_I;
    }

    return VideoStandards.INVALID;
  }

  public getXbeRegion(): XbeRegion {
    const raw = this.get(Structure.XBERegion)[0];
    switch (raw) {
      case XbeRegion.INVALID:
        return XbeRegion.INVALID;
      case XbeRegion.NORTH_AMERICA:
        return XbeRegion.NORTH_AMERICA;
      case XbeRegion.JAPAN:
        return XbeRegion.JAPAN;
      case XbeRegion.EURO_AUSTRALIA:
        return XbeRegion.EURO_AUSTRALIA;
    }
    throw Error('Invalid XBE region');
  }

  public set(slice: ByteSlice, values: Uint8Array) {
    const start = slice[0];

    if (values.length !== slice[1]) {
      throw Error('Invalid values. Expected length does not match the given data.');
    }

    const max = Math.min(slice[1], this.raw.length);
    for (let i = 0; i < max; i++) {
      const byte = values[i] & 0xff;
      const index = start + i;
      this.raw[index] = byte;
    }
  }

  public get(slice: ByteSlice): Uint8Array {
    const start = slice[0];
    const end = start + slice[1];

    if (start < 0 || end < 0) {
      return new Uint8Array();
    }

    if (end === start) {
      return new Uint8Array();
    }

    return this.raw.filter((_, index) => index >= start && index < end);
  }

  public calculateHddKey(model: string, serialNumber: string): string {
    const key = this.get(Structure.HDDKkey);
    const modelBytes = stringToBytes(model);
    const serialNumberBytes = stringToBytes(serialNumber);
    const password = calculateHddPassword(key, modelBytes, serialNumberBytes);
    return arrayToHexString(password);
  }
}
