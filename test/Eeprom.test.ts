import { decryptEeprom, Eeprom } from '../src/Eeprom';
import { XbeRegion } from '../src/Types';
import { ByteSlice, Structure, VideoStandards, XboxVersion } from '../src/Types';

let raw: Uint8Array;
let eeprom: Eeprom;

beforeEach(() => {
  raw = new Uint8Array(256);
  eeprom = new Eeprom(raw);
});

test('Set handles out of bounds input', () => {
  function wrong() {
    const good = Structure.MACAddress;
    eeprom.set(good, new Uint8Array([1, 2, 3, 4, 5, 6, 7]));
  }

  expect(wrong).toThrowErrorMatchingSnapshot();
});

test('Set handles negative array lengths', () => {
  function wrong() {
    const bad: ByteSlice = [1, -2];
    eeprom.set(bad, new Uint8Array([42, 42, 42]));
  }

  expect(wrong).toThrowErrorMatchingSnapshot();
});

test('Set out of eeprom bounds', () => {
  const bad: ByteSlice = [0, 257];

  const data = new Uint8Array(257);
  for (let i = 0; i < 257; i++) {
    data[i] = 1;
  }

  eeprom.set(bad, data);

  const actual = eeprom.get(bad);
  expect(actual.length).toBe(256);
  expect(actual[255]).toBe(1);
});

test('Eeprom size must be 256', () => {
  function lessThan() {
    // tslint:disable-next-line: no-unused-expression
    new Eeprom(new Uint8Array(255));
  }

  function greaterThan() {
    // tslint:disable-next-line: no-unused-expression
    new Eeprom(new Uint8Array(257));
  }

  expect(lessThan).toThrowErrorMatchingSnapshot();
  expect(greaterThan).toThrowErrorMatchingSnapshot();
});

test('HDD key set and get', () => {
  expect(eeprom.getHddKey()).toEqual('00000000000000000000000000000000');

  const key = new Uint8Array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]);
  eeprom.set(Structure.HDDKkey, key);

  expect(eeprom.getHddKey()).toBe('0102030405060708090a0b0c0d0e0f10');
});

test('Bet data boundaries', () => {
  expect(eeprom.get([-1, 0]).length).toBe(0);
  expect(eeprom.get([0, -1]).length).toBe(0);

  expect(eeprom.get([1, 0]).length).toBe(0);
});

test('Read invalid eeprom', () => {
  expect(() => decryptEeprom(raw)).toThrowErrorMatchingSnapshot();
  expect(eeprom.version).toBeUndefined();
});

test('Read actual EEPROM', () => {
  raw = fromHexString(EEPROM_DUMP1);
  expect(raw[0]).toBe(0xac);
  expect(raw.length).toBe(256);

  eeprom = decryptEeprom(raw);
  expect(eeprom.version).toBe(XboxVersion.V1_0);

  const model = 'Quantum FireBird';
  const serialNumber = '00000000000000000001';
  const expectedKey = '2534d6e45c2d709609a0abf072b8f2c8d588c36e';
  expect(eeprom.calculateHddKey(model, serialNumber)).toBe(expectedKey);

  expect(eeprom.getVideoStandard()).toEqual(VideoStandards.PAL_I);
  expect(eeprom.getXbeRegion()).toEqual(XbeRegion.EURO_AUSTRALIA);
});

test('Determine video standard', () => {
  expect(eeprom.getVideoStandard()).toEqual(VideoStandards.INVALID);

  eeprom.set(Structure.VideoStandard, new Uint8Array([0, 2, 3, 0]));
  expect(eeprom.getVideoStandard()).toEqual(VideoStandards.INVALID);

  eeprom.set(Structure.VideoStandard, new Uint8Array(VideoStandards.NTSC_M));
  expect(eeprom.getVideoStandard()).toEqual(VideoStandards.NTSC_M);

  eeprom.set(Structure.VideoStandard, new Uint8Array(VideoStandards.PAL_I));
  expect(eeprom.getVideoStandard()).toEqual(VideoStandards.PAL_I);

  eeprom.set(Structure.VideoStandard, new Uint8Array([1, 0, 0, 1]));
  expect(() => eeprom.getVideoStandard()).toThrowErrorMatchingSnapshot();
});

test('Determine XBE region', () => {
  expect(eeprom.getXbeRegion()).toBe(XbeRegion.INVALID);

  eeprom.set(Structure.XBERegion, new Uint8Array([XbeRegion.NORTH_AMERICA, 0, 0, 0]));
  expect(eeprom.getXbeRegion()).toBe(XbeRegion.NORTH_AMERICA);

  eeprom.set(Structure.XBERegion, new Uint8Array([XbeRegion.JAPAN, 0, 0, 0]));
  expect(eeprom.getXbeRegion()).toBe(XbeRegion.JAPAN);

  eeprom.set(Structure.XBERegion, new Uint8Array([XbeRegion.EURO_AUSTRALIA, 0, 0, 0]));
  expect(eeprom.getXbeRegion()).toBe(XbeRegion.EURO_AUSTRALIA);

  eeprom.set(Structure.XBERegion, new Uint8Array([42, 0, 0, 0]));
  expect(() => eeprom.getXbeRegion()).toThrowErrorMatchingSnapshot();
});

const EEPROM_DUMP1 =
  'ac77ab52c8d1e705b7bbbe46b3640680b9051b8f0ee78d909b280a1fdbb434' +
  'ca6dc670cfc59b583c47237240136505a615ace38f313035373734343232323' +
  '0330050f26943f70000e9b61052a7c0bb9686938245f567c03a0003800000000' +
  '000ea535f53b400000045535354455344540000000000000000020200020a03' +
  '0002000000000000000000000000c4ffffff010000000000090000000000000' +
  '000000000000000000000000000000000000000000000000000000300000' +
  '002000000000000000000000000000000000000000000000000000000000' +
  '00000000000000000000000000000000000000000000000000000000000000000000000000000';

const fromHexString = (hexString: string) => {
  const match = hexString.match(/.{1,2}/g);
  if (match == null) {
    return new Uint8Array();
  }
  return new Uint8Array(match.map(byte => parseInt(byte, 16)));
};
