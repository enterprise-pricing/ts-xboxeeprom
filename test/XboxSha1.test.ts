import { XboxVersion } from '../src/Types';
import { Result, XboxSha1 } from '../src/XboxSha1';

test('Known hash small input', () => {
  const sha1 = new XboxSha1();
  sha1.input(new Uint8Array([1, 3, 3, 7]), 4);

  const expected = [
    4294967284,
    4294964315,
    4294204390,
    4099663378,
    4294967195,
    4294941679,
    4288409509,
    2616173968,
    115,
    29453,
    7540131,
    1930273777,
    0,
    238,
    61115,
    15645554,
    23,
    6102,
    1562289,
    399946144,
  ];

  expect(sha1.digest()).toEqual(new Uint32Array(expected));
});

test('Known hash medium input', () => {
  const sha1 = new XboxSha1();
  sha1.reset();

  const len = 56;
  const input = new Uint8Array(len);
  for (let i = 0; i < len; i++) {
    input[i] = i;
  }

  sha1.input(input, input.length);

  const expected = [
    99,
    25454,
    6516270,
    1668165318,
    4294967192,
    4294940890,
    4288207561,
    2564475139,
    73,
    18830,
    4820580,
    1234068619,
    4294967250,
    4294955763,
    4292015023,
    3539185508,
    29,
    7484,
    1916040,
    490506443,
  ];

  expect(sha1.digest()).toEqual(new Uint32Array(expected));
  expect(sha1.input(input, input.length)).toBe(Result.SHA_STATE_ERROR);
});

test('Known hash large input', () => {
  const sha1 = new XboxSha1();
  sha1.reset();

  const len = 1024;
  const input = new Uint8Array(len);
  for (let i = 0; i < len; i++) {
    input[i] = i;
  }

  sha1.input(input, input.length);

  const expected = [
    91,
    23296,
    5963878,
    1526752924,
    72,
    18445,
    4722012,
    1208835327,
    4294967229,
    4294950394,
    4290640523,
    3187313627,
    4294967209,
    4294945173,
    4289303905,
    2845139222,
    15,
    3885,
    994587,
    254614391,
  ];

  expect(sha1.digest()).toEqual(new Uint32Array(expected));
});

test('Very long input', () => {
  const sha1 = new XboxSha1();
  sha1.reset();

  const len = 1;
  const input = new Uint8Array(len);
  for (let i = 0; i < len; i++) {
    input[i] = i;
  }

  expect(sha1.context.corrupted).toBe(Result.SHA_SUCCESS);

  sha1.context.lengthLow = 0xffffffff - 7;
  sha1.context.lengthHigh = 0xffffffff;

  sha1.input(input, input.length);

  expect(sha1.context.corrupted).toBe(Result.SHA_NULL);

  const expected = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  expect(sha1.digest()).toEqual(new Uint32Array(expected));
  expect(sha1.input(input, input.length)).toBe(Result.SHA_NULL);
});

test('HMAC reset 1, XboxVersion.NONE', () => {
  const sha1 = new XboxSha1();
  expect(sha1.context.lengthLow).toBe(0);

  sha1.hmac1reset(XboxVersion.NONE);

  const intermediate = sha1.context.intermediateHash;
  expect(intermediate[0]).toBe(0x85f9e51a);
  expect(intermediate[1]).toBe(0xe04613d2);
  expect(intermediate[2]).toBe(0x6d86a50c);
  expect(intermediate[3]).toBe(0x77c32e3c);
  expect(intermediate[4]).toBe(0x4bd717a4);

  expect(sha1.context.lengthLow).toBe(512);
});

test('HMAC reset 1, XboxVersion.V1_0', () => {
  const sha1 = new XboxSha1();
  expect(sha1.context.lengthLow).toBe(0);

  sha1.hmac1reset(XboxVersion.V1_0);

  const intermediate = sha1.context.intermediateHash;
  expect(intermediate[0]).toBe(0x72127625);
  expect(intermediate[1]).toBe(0x336472b9);
  expect(intermediate[2]).toBe(0xbe609bea);
  expect(intermediate[3]).toBe(0xf55e226b);
  expect(intermediate[4]).toBe(0x99958dac);

  expect(sha1.context.lengthLow).toBe(512);
});

test('HMAC reset 1, XboxVersion.V1_1', () => {
  const sha1 = new XboxSha1();
  expect(sha1.context.lengthLow).toBe(0);

  sha1.hmac1reset(XboxVersion.V1_1);

  const intermediate = sha1.context.intermediateHash;
  expect(intermediate[0]).toBe(0x39b06e79);
  expect(intermediate[1]).toBe(0xc9bd25e8);
  expect(intermediate[2]).toBe(0xdbc6b498);
  expect(intermediate[3]).toBe(0x40b4389d);
  expect(intermediate[4]).toBe(0x86bbd7ed);

  expect(sha1.context.lengthLow).toBe(512);
});

test('HMAC reset 1, XboxVersion.V1_2', () => {
  const sha1 = new XboxSha1();
  expect(sha1.context.lengthLow).toBe(0);

  sha1.hmac1reset(XboxVersion.V1_2);

  const intermediate = sha1.context.intermediateHash;
  expect(intermediate[0]).toBe(0x8058763a);
  expect(intermediate[1]).toBe(0xf97d4e0e);
  expect(intermediate[2]).toBe(0x865a9762);
  expect(intermediate[3]).toBe(0x8a3d920d);
  expect(intermediate[4]).toBe(0x08995b2c);

  expect(sha1.context.lengthLow).toBe(512);
});

test('HMAC reset 2, XboxVersion.NONE', () => {
  const sha1 = new XboxSha1();
  expect(sha1.context.lengthLow).toBe(0);

  sha1.hmac2reset(XboxVersion.NONE);

  const intermediate = sha1.context.intermediateHash;
  expect(intermediate[0]).toBe(0x5d7a9c6b);
  expect(intermediate[1]).toBe(0xe1922beb);
  expect(intermediate[2]).toBe(0xb82ccdbc);
  expect(intermediate[3]).toBe(0x3137ab34);
  expect(intermediate[4]).toBe(0x486b52b3);

  expect(sha1.context.lengthLow).toBe(512);
});

test('HMAC reset 2, XboxVersion.V1_0', () => {
  const sha1 = new XboxSha1();
  expect(sha1.context.lengthLow).toBe(0);

  sha1.hmac2reset(XboxVersion.V1_0);

  const intermediate = sha1.context.intermediateHash;
  expect(intermediate[0]).toBe(0x76441d41);
  expect(intermediate[1]).toBe(0x4de82659);
  expect(intermediate[2]).toBe(0x2e8ef85e);
  expect(intermediate[3]).toBe(0xb256faca);
  expect(intermediate[4]).toBe(0xc4fe2de8);

  expect(sha1.context.lengthLow).toBe(512);
});

test('HMAC reset 2, XboxVersion.V1_1', () => {
  const sha1 = new XboxSha1();
  expect(sha1.context.lengthLow).toBe(0);

  sha1.hmac2reset(XboxVersion.V1_1);

  const intermediate = sha1.context.intermediateHash;
  expect(intermediate[0]).toBe(0x9b49bed3);
  expect(intermediate[1]).toBe(0x84b430fc);
  expect(intermediate[2]).toBe(0x6b8749cd);
  expect(intermediate[3]).toBe(0xebfe5fe5);
  expect(intermediate[4]).toBe(0xd96e7393);

  expect(sha1.context.lengthLow).toBe(512);
});

test('HMAC reset 2, XboxVersion.V1_2', () => {
  const sha1 = new XboxSha1();
  expect(sha1.context.lengthLow).toBe(0);

  sha1.hmac2reset(XboxVersion.V1_2);

  const intermediate = sha1.context.intermediateHash;
  expect(intermediate[0]).toBe(0x01075307);
  expect(intermediate[1]).toBe(0xa2f1e037);
  expect(intermediate[2]).toBe(0x1186eeea);
  expect(intermediate[3]).toBe(0x88da9992);
  expect(intermediate[4]).toBe(0x168a5609);

  expect(sha1.context.lengthLow).toBe(512);
});

test('Uint8 overflow', () => {
  const array = new Uint8Array(1);
  array[0] = 257;
  expect(array[0]).toBe(1);
});

test('Uint32 overflow', () => {
  const array = new Uint32Array(1);
  array[0] = 0xffffffff + 2;
  expect(array[0]).toBe(1);
});
